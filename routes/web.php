<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/pendaftaran', [App\Http\Controllers\AuthController::class, 'index'])->name('register');
Route::post('/kirim', [App\Http\Controllers\AuthController::class, 'welcome'])->name('welcome');
Route::get('/dataTable', [App\Http\Controllers\IndexController::class, 'index'])->name('dataTable');

Route::get('/cast', [App\Http\Controllers\CastController::class, 'index'])->name('cast');
Route::get('/cast/create', [App\Http\Controllers\CastController::class, 'create'])->name('crateCast');
Route::post('/cast', [App\Http\Controllers\CastController::class, 'store'])->name('storeCast');
Route::get('/cast/{cast_id}', [App\Http\Controllers\CastController::class, 'show'])->name('showCast');
Route::get('/cast/{cast_id}/edit', [App\Http\Controllers\CastController::class, 'edit'])->name('editCast');
Route::put('/cast/{cast_id}', [App\Http\Controllers\CastController::class, 'update'])->name('updateCast');
Route::delete('/cast/{cast_id}', [App\Http\Controllers\CastController::class, 'destroy'])->name('destroyCast');
