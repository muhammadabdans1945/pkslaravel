<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function index()
    {
        $data = array(
            'title' => 'Halaman Form'
        );
        return view('form',compact('data'));
    }
    public function welcome(Request $req)
    {
        $data = array(
            'nama' => $req->fName.' '.$req->lName,
            'title' => 'Halaman Index'
        );
        return view('dashboard',compact('data'));
    }
}
