<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    //
    public function index()
    {
        $data = array(
            'title' => 'Data Table'
        );
        return view('dataTable',compact('data'));
    }
}
