<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cast;

class CastController extends Controller
{
    public function index()
    {
        $data = array(
            'title' => 'Halaman Cast'
        );
        $cast = Cast::all();
        return view('cast.index', compact('cast', 'data'));
    }

    public function create()
    {
        $data = array(
            'title' => 'Halaman Create Casts'
        );
    	return view('cast.create', compact('data'));
    }
 
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'nama' => 'required',
    		'umur' => 'required',
    		'bio' => 'required'
    	]);
 
        Cast::create([
    		'nama' => $request->nama,
    		'umur' => $request->umur,
    		'bio' => $request->bio
    	]);
 
    	return redirect('/cast');
    }
    public function show($id)
    {
        $data = array(
            'title' => 'Halaman Create Casts'
        );
        $cast = Cast::find($id);
        return view('cast.show', compact('cast', 'data'));
    }
    public function edit($id)
    {
        $data = array(
            'title' => 'Halaman Edit Casts'
        );
        $cast = Cast::find($id);
        return view('cast.edit', compact('cast', 'data'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $cast = cast::find($id);
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $cast->update();
        return redirect('/cast');
    }
    public function destroy($id)
    {
        $cast = Cast::find($id);
        $cast->delete();
        return redirect('/cast');
    }
}
